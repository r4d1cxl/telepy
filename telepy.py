from datetime import datetime
import sys
import telepot
import socket
import time
import struct
import subprocess

sockt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sockt.bind(('0.0.0.0', 0))

now = datetime.now()
mytime = now.strftime("%H:%M:%S")

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.connect(("8.8.8.8", 80))



def system_call(command):
    p = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)
    return p.stdout.read()

def get_gateway_address():
    return ( "\nGateway: " + str(system_call("ip route show 0.0.0.0/0 2>/dev/null | cut -d\  -f3")))

real_ip_pub = "\nYour Public Ip: " + str(system_call("dig +short myip.opendns.com @resolver1.opendns.com"))
gateway = get_gateway_address()
gotcha = "Time: " + str(mytime) + real_ip_pub + "\nWe are currently at: " + str(sock.getsockname()[0]) + ':' + str(sockt.getsockname()[1]) + str(gateway)


def handle(msg):
    chat_id = msg['chat'] ['id']
    command = msg['text']

    if command == '/watch' or command == '/locate':
        bot.sendMessage (chat_id, gotcha)

    elif command == '/start':
            bot.sendMessage (chat_id, "Welcome to Telepy\nI am a simple bot, created by Darwin Castro and his brother, Daniel Castro\nI will help you to know:\nThe current ip\nThe _gateway\nAnd current time\non your little Raspberry Pi\nYou can call me whenever you want with the /watch or the /locate commands.\nOh, and remember...\nI'm still in development so, expect more features soon")

    else:
        bot.sendMessage (chat_id, "That's not a valid command.\nPlease, use /watch or /locate.")

bot = telepot.Bot ('****')
bot.message_loop(handle)
print("I'm Listening...")

while 1:
    time.sleep(10)
