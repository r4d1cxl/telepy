import socket
from datetime import datetime
import struct
import subprocess

sockt = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
sockt.bind(('0.0.0.0', 0)) 

now = datetime.now()
mytime = now.strftime("%H:%M:%S")

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.connect(("8.8.8.8", 80))

gotcha = "nock nock, we currently at: " + str(sock.getsockname()[0]) + ':' + str(sockt.getsockname()[1])

def system_call(command):
    p = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)
    return p.stdout.read()

def get_gateway_address():
    return ( "gates gates, catching gates... " + str(system_call(" ip route show 0.0.0.0/0 2>/dev/null | cut -d\  -f3")))

gateway = get_gateway_address()

print (gotcha + "\n" + str(gateway) +"\n" + "On: " +str(mytime))
