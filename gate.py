import socket, struct
import subprocess
def system_call(command):
    p = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)
    return p.stdout.read()

def get_gateway_address():
    print ( "Your Gateway: " + str(system_call("route -n | grep 'UG[ \t]' | awk '{print $2}' ")))
    print ( "Through SubMask: " + str(system_call('/sbin/ifconfig 2>/dev/null | grep Mask | cut -d":" -f4')))
gateway = get_gateway_address()
print (gateway)
